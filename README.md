# README #

Application was created like test task for position Python Intern. Here you can find page with chart of rate UAH to USD and possibility to add rate for any day and if on this day rate present than on this date rate will updated. 

### Used technologies ###

* Python 3.8.5
* Flask 1.1.2
* WTForms 2.3.3
* SQLAlchemy 1.3.22
* JavaScript
* Bootstrap 5.0.0
* HTML
* CSS

### Structure ###
```
├── rate_history/
│   ├── __init__.py
│   ├── forms.py
│   ├── models.py
│   ├── routes.py
│   ├── templates/
│   │   ├── base.html
│   │   └── index.html
│   └── static/
│       └── js/
│           └── plot_chart.js
├── venv/
├── config.py
├── README.md
├── app.py
└── requirements.txt
```
### Install and run project ###
```
git clone https://zelenyid@bitbucket.org/zelenyid/rate_history.git
cd rate_history
sourse venv/bin/activate
pip install -r requirements.txt.
```
- Next you need to set environment variables: SECRET_KEY and SQLALCHEMY_DATABASE_URI in config
- SECRET_KEY you can give next steps in python interpreter:
```
    import secrets
    sectets.token_hex(16)
```
- SQLALCHEMY_DATABASE_URI it's uri to your database

- For running it on your local machine you need to run next commant
```
python app.py
```
