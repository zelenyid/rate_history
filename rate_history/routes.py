import json
import operator

from flask import render_template, flash
from sqlalchemy.exc import DataError

from rate_history import app, db
from .forms import RateForm
from .models import RateData


def db_session_add(obj):
    """
    Add to database an object
    :param obj: object for adding to db
    :return: None
    """
    try:
        db.session.add(obj)
        db.session.commit()
        flash('Data was successfully added', 'success')
    except DataError:
        db.session.rollback()
        flash('Check your data again. You have a mistake', 'danger')


def db_session_delete(query, obj_id):
    """
    Delete object from db
    :param query: Model
    :param obj_id:
    :return: None
    """
    try:
        del_obj = db.session.query(query).get(obj_id)
        db.session.delete(del_obj)
        db.session.commit()
    except DataError:
        db.session.rollback()


def add_rate(date, rate_uah_to_usd):
    """
    Add rate to db
    :param date: date of rate
    :param rate_uah_to_usd: UAH to USD
    :return: None
    """
    new_rate = RateData(date=date, rate_uah_to_usd=rate_uah_to_usd)

    old_rate = RateData.query.filter_by(date=date).first()

    if old_rate:
        db_session_delete(RateData, old_rate.id)

    db_session_add(new_rate)


@app.route('/', methods=['GET', 'POST'])
def index():
    """
    Generate start page with form and chart
    :return: index.html template with parameters
    """
    form = RateForm()

    if form.validate_on_submit():
        add_rate(date=form.date.data, rate_uah_to_usd=form.rate_uah_to_usd.data)

    if form.errors:
        flash('Check your data again. You have a mistake', 'danger')

    rate_list = sorted(list(db.session.query(RateData)), key=operator.attrgetter('date'))
    plot_data = [[rate.date.strftime('%m/%d/%Y'), rate.rate_uah_to_usd] for rate in rate_list]

    return render_template('index.html', form=form, data=json.dumps(plot_data), display_grapth=bool(plot_data))
