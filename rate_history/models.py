from rate_history import db


class RateData(db.Model):
    """
    Model to storage rate UAH to USD by date
    """
    __tablename__ = 'rate_data'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, unique=True, nullable=False)
    rate_uah_to_usd = db.Column(db.Float, nullable=False)

    def __repr__(self):
        return f'{self.date} - {self.rate_uah_to_usd} UAH for 1 USD'
