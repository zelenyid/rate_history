from flask_wtf import FlaskForm
from wtforms import SubmitField, FloatField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, InputRequired


class RateForm(FlaskForm):
    """
    Class form for adding new rate to database
    """
    date = DateField('Date', validators=[DataRequired(), InputRequired()])
    rate_uah_to_usd = FloatField('Rate UAH to USD', validators=[DataRequired(), InputRequired()])

    add_btn = SubmitField('Add')
