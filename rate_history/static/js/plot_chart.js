google.charts.load('current', {'packages': ['line']});
google.charts.setOnLoadCallback(drawChart);

// Function to draw chart by data from database
function drawChart() {
    let data = new google.visualization.DataTable();

    data.addColumn('string', 'Date');
    data.addColumn('number', 'UAH to USD');

    let script_tag = document.getElementById('chart_js');
    data.addRows(JSON.parse(script_tag.getAttribute("data")));

    let options = {
        width: 900,
        height: 500,
        axes: {
            x: {
                0: {side: 'top'}
            }
        }
    };

    let chart = new google.charts.Line(document.getElementById('line_top_x'));

    chart.draw(data, google.charts.Line.convertOptions(options));
}
